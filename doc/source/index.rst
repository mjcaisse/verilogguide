.. FPGA designs with Verilog documentation master file, created by
   sphinx-quickstart on Thu Nov  2 19:09:33 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

FPGA designs with Verilog
=========================

.. toctree::
    :numbered:
    :maxdepth: 3
    :caption: Contents:

    verilog/firstproject
    verilog/overview
    verilog/datatype
    verilog/procedure
    verilog/vhdl
    verilog/vvd
    verilog/fsm
    verilog/designs
    verilog/testbench
    verilog/systemverilog
    verilog/package
    verilog/interface
    verilog/niosoverview
    verilog/niosread
    verilog/uart
    verilog/appendix
    verilog/app_niosoverview


.. Indices and tables
.. ==================

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`
